﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hoverscript : MonoBehaviour {

	public enum state{
		inactive,
		active
	};

	state ActiveState = state.inactive;

	public Sprite nonHoverSprite;
	public Sprite hoverSprite;
	SpriteRenderer myRenderer;

	bool hoverable = false;
	bool isActive = false;


	// Use this for initialization
	void Start () {
		myRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {

		switch(ActiveState){
		case state.active:
			myRenderer.sprite = hoverSprite;
			hoverable = true;
			break;
		case state.inactive:
			break;

		}

		if(!hoverable && myRenderer.sprite != nonHoverSprite){
			myRenderer.sprite = nonHoverSprite;
		}

	}

	void OnMouseOver(){
		if(hoverable){
			myRenderer.sprite = hoverSprite;
		}
	}

	void OnMouseExit(){
		if(hoverable){
			myRenderer.sprite = nonHoverSprite;
		}
	}

	public void changeHover(bool boolean){
		hoverable = boolean;
	}

	public void Active(bool boolean){
		isActive = boolean;

		if(isActive){
			ActiveState = state.active;
		}else{
			ActiveState = state.inactive;
			myRenderer.sprite = nonHoverSprite;
		}
	}

}
